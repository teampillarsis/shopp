Shopp!
======
Shopping cart is supermarket shopping cart on a vendor's website which serves to keep the list of items a user (buyer) has 'picked up' from the online store. When the buyer is ready to 'check out,' he or she may go through the list to make a final purchase decision before clicking the 'buy' button.
